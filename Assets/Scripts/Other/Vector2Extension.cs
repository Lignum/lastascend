﻿using UnityEngine;

public static class Vector2Extension {

    public static Vector2 Rotate(this Vector2 v, float degrees) {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    public static Vector2 Limit(this Vector2 v, Vector2 limit, Vector2 up) {
        if(Vector2.Angle(up, v) >= 90) {
            if(Vector2.Angle(limit, v) <= 90) {
                v = v.Rotate(Vector2.Angle(limit, v));
            }else {
                v = -v.Rotate(Vector2.Angle(limit, v));
            }
        }


        return v;
    }

    public static Vector2 Limit(this Vector2 v, Vector2 up) {

        Vector2 limit = Vector3.Cross(up, Vector3.forward);
        if(Vector2.Angle(up, v) >= 90) {
            if(Vector2.Angle(limit, v) <= 90) {
                v = v.Rotate(Vector2.Angle(limit, v));
            } else {
                v = -v.Rotate(Vector2.Angle(limit, v));
            }
        }


        return v;
    }

}