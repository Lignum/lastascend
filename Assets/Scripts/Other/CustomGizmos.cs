﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CustomGizmos {
    
    public static void drawRectagle(Vector2 position, Vector2 size) {

        Gizmos.color = Color.red;
        Gizmos.DrawRay(position, Vector2.up * size.y);
        Gizmos.DrawRay(position, Vector2.right * size.x);

        Gizmos.DrawRay(position + size, Vector2.down * size.y);
        Gizmos.DrawRay(position + size, Vector2.left * size.x);


    }

}
