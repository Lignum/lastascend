﻿using LastAscend.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastAscend.Physics {
    public struct Corner  {

        public static Corner EMPTY = new Corner(Vector2.zero, Vector2.zero, Vector2.zero);

        public Vector2 Anchor { get; private set; }
        public Vector2 Pre { get; private set; }
        public Vector2 Next { get; private set; }

        public bool IsOnWall { get; private set; }

        public bool IsValid { get; private set; }

        public Vector2 Ground { get; private set; }
        public Vector2 GroundDirection { get { return Ground.normalized; } }
        public Vector2 GroundNormal { get; private set; }
        public float GroundWidth { get { return Mathf.Abs(Ground.x); } }

        public Vector2 WallNormal { get; private set; }

        //public Corner(Vector3 anchor, Vector3 pre, Vector3 next) : this((Vector2)anchor, (Vector2)pre, (Vector2)next) {}
        public Corner(Vector2 anchor, Vector2 pre, Vector2 next) {
            Anchor = anchor;
            Pre = pre;
            Next = next;

            IsOnWall = false;

            IsValid = Vector2.Distance(next, anchor) != 0;

            Ground = Vector2.zero;
            GroundNormal = Vector2.zero;
            WallNormal = Vector2.zero;

            IsOnWall = isOnWall(Anchor, Pre, Next);


        }

        bool isOnWall(Vector2 corner, Vector2 pre, Vector2 next) {

            Vector2 dir1 = (pre - corner).normalized;
            Vector2 dir2 = (next - corner).normalized;

            Vector2 wallPoint = Vector2.zero;
            Vector2 groundPoint = Vector2.zero;

            if(Vector2.Angle(dir1, Vector2.up) > 150) {
                wallPoint = pre;
                groundPoint = next;
            } else if(Vector2.Angle(dir2, Vector2.up) > 150) {
                wallPoint = next;
                groundPoint = pre;
            }

            if(wallPoint == Vector2.zero) return false;

            int dir = 1;
            if(wallPoint.x < groundPoint.x) {
                dir = -1;
            }

            WallNormal = (wallPoint - corner).normalized.Rotate(90 * dir);

            Ground = (groundPoint - corner);
            GroundNormal = GroundDirection.Rotate(-90 * dir);

            return Vector2.Distance(corner, wallPoint) > .3f;

        }

        public void drawGizmos() {

            Gizmos.color = Color.red;
            Gizmos.DrawRay(Anchor, WallNormal);

            Gizmos.color = Color.green;
            Gizmos.DrawRay(Anchor, GroundNormal);

        }
    }
}