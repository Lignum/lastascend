﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace LastAscend.Physics {
    public class CornerDetector {

        float _size;
        float _maxAngle;

        float _minDistance;
        float _distance;

        List<Corner> _detectedCorners;
        Collider2D[] _colliders;

        public CornerDetector(float size, float maxAngle) {
            _size = size;
            _maxAngle = maxAngle;

            _detectedCorners = new List<Corner>();
        }

        public Corner cast(Vector2 position) {
            /*
            Debug.DrawRay(position, Vector2.right * _size);
            Debug.DrawRay(position, Vector2.up * _size);
            Debug.DrawRay(position, Vector2.down * _size);
            Debug.DrawRay(position, Vector2.left * _size);
            */
            _colliders = Physics2D.OverlapCircleAll(position, _size);
            _minDistance = _size + .1f;

            Corner validCorner = Corner.EMPTY;
            int[] points;
            Mesh mesh;

            foreach(var collider in _colliders) {

                _detectedCorners.Clear();

                mesh = collider.CreateMesh(true, true);

                for(int i = 0; i < mesh.vertices.Length; i++) {

                    points = getSidePointsOf(i, mesh.triangles);
                        
                    if(points.Length == 2 && tryCreateCorner(mesh.vertices[i], mesh.vertices[points[0]], mesh.vertices[points[1]], out Corner created)) {
                        _detectedCorners.Add(created);
                    }
                }

                for(int i = 0; i < _detectedCorners.Count; i++) {

                    if(Vector2.Distance(position, _detectedCorners[i].Anchor) < _minDistance) {

                        validCorner = _detectedCorners[i];
                        _minDistance = Vector2.Distance(position, _detectedCorners[i].Anchor);
                    }
                }
            }

            return validCorner;
        }

        int[] getSidePointsOf(int vertexPos, int[] triangles) {

            Dictionary<int, int> containingT = new Dictionary<int, int>();
            HashSet<int> uniquePoints = new HashSet<int>();

            for(int i = 0; i < triangles.Length; i+=3) {

                if(triangles[i] == vertexPos || triangles[i+1] == vertexPos || triangles[i+2] == vertexPos) {
                    for(int e = 0; e < 3; e++) {
                        if(containingT.ContainsKey(triangles[i + e])) {
                            containingT[triangles[i + e]]++;
                        } else {
                            containingT.Add(triangles[i + e], 1);
                        }
                    }
                }
            }

            foreach(var item in containingT) {

                if(item.Key == vertexPos) continue;

                if(item.Value == 1) {
                    uniquePoints.Add(item.Key);
                }
            }

            return uniquePoints.ToArray();
            

        }

        bool tryCreateCorner(Vector2 corner, Vector2 pre, Vector2 next, out Corner created) {

            if(!isValidCorner(corner, pre, next)) {                
                created = Corner.EMPTY;
                return false;
            }

            created = new Corner(corner, pre, next);
            return true;
        }

        bool isValidCorner(Vector2 corner, Vector2 pre, Vector2 next) {
            //Debug.DrawLine(corner, pre, Color.red);
            //Debug.DrawLine(corner, next, Color.green);

            Vector2 dir1 = (pre - corner).normalized;
            Vector2 dir2 = (next - corner).normalized;

            if(Vector2.Angle(dir1, dir2) > _maxAngle) return false;
            if(-(dir1 + dir2).normalized.y < 0) return false;
            if(Vector2.Angle(-(dir1 + dir2), Vector2.up) < 30) return false;

            return true;

        }

        

    }
}