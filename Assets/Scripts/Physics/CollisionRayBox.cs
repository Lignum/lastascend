﻿using System.Collections.Generic;
using UnityEngine;

namespace LastAscend.Physics {
    public struct CollisionRayBox {

        public RaycastHit2D[] Hits { get; private set; }
        public int NumOfRays {
            get {
                return Hits.Length;
            }
        }

        float _size;
        float _increment;

        public CollisionRayBox(float size, int raysNumber) {
            Hits = new RaycastHit2D[raysNumber];
            _size = size;

            _increment = _size / (Hits.Length - 1);
        }

        public void setSize(float size) {
            _size = size;

            _increment = _size / (Hits.Length - 1);
        }

        public void cast(Vector2 position, Vector2 direction, float distance) {
            for(int i = 0; i < Hits.Length; i++) {
                Hits[i] = Physics2D.Raycast(position + direction.Rotate(90) * (_increment * i - (_size / 2)), direction, distance);
            }
        }

        public HashSet<RaycastHit2D> cast(Vector2 position, float distance, params Vector2[] directions) {
            HashSet<RaycastHit2D> hits = new HashSet<RaycastHit2D>();
            foreach(var dir in directions) {
                for(int i = 0; i < Hits.Length; i++) {
                    hits.Add(Physics2D.Raycast(position + dir.Rotate(90) * (_increment * i - (_size / 2)), dir, distance));

                    Debug.DrawRay(position + dir.Rotate(90) * (_increment * i - (_size / 2)), dir * distance, Color.yellow);
                }
            }

            return hits;
        }

        public void drawDebug(Vector2 position, Vector2 direction, float distance, Color color) {
            for(int i = 0; i < Hits.Length; i++) {
                Debug.DrawRay(position + direction.Rotate(90) * (_increment * i - (_size / 2)), direction * distance, color);
            }
        }
    }
}
