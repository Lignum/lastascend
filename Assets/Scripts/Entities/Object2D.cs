﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastAscend.Entities {
    public class Object2D : MonoBehaviour {

        public Vector2 Position {
            get {
                return transform.position;
            }
            set {
                transform.position = value;
            }
        }
        public Vector2 LocalPosition {
            get {
                return transform.localPosition;
            }
            set {
                transform.localPosition = value;
            }
        }

        public Vector2 BodyUp {
            get {
                return transform.up;
            }
        }
        public Vector2 BodyRight {
            get {
                return transform.right;
            }
        }
    }
}