﻿using LastAscend.Physics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastAscend.Entities {
    public class PhysicEntity : Object2D {

        const float GRAVIY = 10;

        const float CLIMB_UP_TIME = .55f;
        const float CLIMB_MOVE_TIME = .15f;
        const float CLIMB_STAND_TIME = .30f;

        [Header("Size")]
        [SerializeField]
        float _height = 1;
        [SerializeField]
        float _width = .2f;
        [SerializeField]
        float _crouchHeight = .2f;
        [SerializeField]
        float _crouchWidth = .4f;

        float _crouchValue = 0;
        float CrouchValue {
            get { return _crouchValue; }
            set {
                _crouchValue = value;
                updateSizes();
            }
        }
        public float UseHeight { get; private set; }
        public float UseWidth { get; private set; }

        [Header("Grab Corners")]
        [SerializeField]
        float _grabPointHeight = .3f;
        float _usePointHeight {
            get { return UseHeight * _grabPointHeight / _height; }
        }
        [SerializeField]
        float _grabDistance = .2f;

        [Header("Movement")]
        [SerializeField]
        float _maxRunSpeed = 10;
        [SerializeField]
        float _drag = 10;

        [HideInInspector]
        public bool GrabToEdges = false;

        float _climbingVal;
        public bool IsClimbing { get { return _climbingVal > 0; } }
        Vector2 _climbStartPos;
        Vector2 _climbTopPos;
        Vector2 _climbEndPos;



        public Vector2 NextFramePosition {
            get { return Position + Velocity * Time.deltaTime; }
        }
 

        public Vector2 Velocity { get; private set; } = Vector2.zero;
        public Vector2 LocalVelocity {
            get {
                return Velocity.Rotate(Vector2.SignedAngle(BodyUp, Vector2.up));
            }
        }
        int _lookDirection = 1;
        Vector2 _direction = Vector2.zero;
        Vector2 _internalForces = Vector2.zero;
        Vector2 _externalForce = Vector2.zero;
        float _inputForce = 0;

        int _movingRight {
            get {
                if(LocalVelocity.x == 0) return 0;
                else return (int)(Mathf.Abs(LocalVelocity.x) / LocalVelocity.x);
            }
        }
        int _movingUp {
            get {
                if(Velocity.y == 0) return -1;
                else return (int)(Mathf.Abs(Velocity.y) / Velocity.y);
            }
        }

        Vector2 _groundCollision;
        public bool IsGrounded {
            get {
                return _groundCollision.magnitude != 0;
            }
        }
        public bool IsLanding { get; private set; }

        Collider2D _wallHit;
        public bool IsTouchingWall {
            get {
                return _wallHit != null;
            }
        }


        public float TimeSinceGrounded { get; private set; }

        RaycastHit2D _hit;
        CollisionRayBox _sideRayBox;
        CornerDetector _cornerDetector;

        Corner _grabedAnchor = Corner.EMPTY;
        float _timeGrabed;
        public bool IsGrabbed {
            get { return _grabedAnchor.IsValid; }
        }
        bool _isReleasingGrab = false;

        bool _hasNoVelocity {
            get {
                return Velocity.magnitude < .001f;
            }
        }

        // Start is called before the first frame update
        void Start() {

            _cornerDetector = new CornerDetector(_grabDistance, 130);
            _sideRayBox = new CollisionRayBox(_height / 2, 20);
        }

        // Update is called once per frame
        void Update() {

            _isReleasingGrab = false;
            IsLanding = false;

            recoverRotation();

            calculateGravity();
            
            checkGrabCorners();
            
            if(_isReleasingGrab) {
                releaseGrab();
            }
            
            applyRunDirection();
            
            limitDirections();

            applyDrag();
            
            applyVelocity();

            if(!IsGrounded) {
                TimeSinceGrounded += Time.deltaTime;
            } else {
                TimeSinceGrounded = 0;
            }

            _direction = Vector2.zero;
        }

        private void OnValidate() {
            UseHeight = Mathf.Lerp(_height, _crouchHeight, _crouchValue);
            UseWidth = Mathf.Lerp(_width, _crouchWidth, _crouchValue);
        }

        void recoverRotation() {
            transform.up = Vector2.Lerp(transform.up, Vector2.up, .2f);
        }
        
        public void run(float dir) {
            _inputForce += dir * Time.deltaTime;
            _direction = BodyRight * dir * Time.deltaTime;

            if(_direction.x * Velocity.x >= 1 && Mathf.Abs(_direction.x + Velocity.x) > _maxRunSpeed) {
                _direction.x = (_maxRunSpeed - Mathf.Abs(Velocity.x)) * dir/Mathf.Abs(dir);
            }

            if(Velocity.x + _direction.x > 0) _lookDirection = 1;
            else if(Velocity.x + _direction.x != 0) _lookDirection = -1;
        }

        public void addForce(Vector2 force) {
            _externalForce += force ;
            _inputForce += force.magnitude;

            TimeSinceGrounded = 100;
        }

        public void cancelVelocity(Vector2 dir, float multiplier = 1) { 
            Velocity -= (Vector2)Vector3.Project(Velocity, dir.normalized) * multiplier;
        }

        public void crouch(float speed) {
            setCrouch(CrouchValue + speed * Time.deltaTime);
        }
        public void setCrouch(float val) {
            CrouchValue = Mathf.Clamp01(val);
        }

        public void addClimb(float speed) {

            if(IsGrabbed || IsClimbing) {
               
                if(_climbingVal == 0) {
                    startClimb();
                }

                Debug.DrawLine(_climbStartPos, _climbTopPos, Color.green);
                Debug.DrawLine(_climbTopPos, _climbEndPos, Color.green);

                _climbingVal = Mathf.Clamp01(_climbingVal + speed * Time.deltaTime);

                if(_climbingVal <= CLIMB_UP_TIME) {
                    setCrouch(Mathf.Max(_crouchValue, Mathf.Lerp(0, 1, _climbingVal / CLIMB_UP_TIME)));
                    Position = Vector2.Lerp(_climbStartPos, _climbTopPos, _climbingVal / CLIMB_UP_TIME);
                } else if(_climbingVal <= CLIMB_UP_TIME + CLIMB_MOVE_TIME){
                    Position = Vector2.Lerp(_climbTopPos, _climbEndPos, (_climbingVal - CLIMB_UP_TIME) / CLIMB_MOVE_TIME);
                } else {
                    setCrouch(Mathf.Lerp(1, 0, (_climbingVal - (CLIMB_UP_TIME + CLIMB_MOVE_TIME)) / CLIMB_STAND_TIME));
                }

                if(_climbingVal >= 1) {
                    stopClimb();
                }
            }
        }

        void startClimb() {
            _climbStartPos = Position;
            Velocity = Vector2.zero;

            int dir = 1;
            if(_grabedAnchor.WallNormal.x > 0) dir = -1;

            _climbTopPos = new Vector2(_grabedAnchor.Anchor.x + (-dir * _width/2), _grabedAnchor.Anchor.y + _crouchHeight / 2);

            float width = _width;
            if(_grabedAnchor.GroundWidth < _width / 2) {
                width = _grabedAnchor.GroundWidth;                
            }

            _climbEndPos = new Vector2(_grabedAnchor.Anchor.x + width / 2 * dir, _grabedAnchor.Anchor.y + _crouchHeight / 2);

        }

        public void stopClimb() {

            _climbStartPos = Vector2.zero;
            _climbingVal = 0;
            clearGrabedAnchor();
        }

        void updateSizes() {
            UseHeight = Mathf.Lerp(_height, _crouchHeight, _crouchValue);
            UseWidth = Mathf.Lerp(_width, _crouchWidth, _crouchValue);

            _sideRayBox.setSize(UseHeight / 2);
        }

        void calculateGravity() {
            if(!IsGrounded && !IsClimbing) {
                _internalForces.y = GRAVIY * -Time.deltaTime;
            } else {
                _internalForces.y = 0;
            }
        }

        void applyRunDirection() {
            if(IsGrounded) {
                Velocity = (Vector2)Vector3.Project(Velocity + _direction + _internalForces, Vector3.Cross(_groundCollision, Vector3.forward));
            } else {
                Velocity += _direction + _internalForces;
            }

            Velocity += _externalForce;
            _externalForce = Vector2.zero;

        }

        void limitDirections() {
            if(IsGrabbed && !IsClimbing) {
                limitGrabbedDirection();
            }

            if(!IsClimbing) {
                if(_movingRight == 0) {
                    castSideCollisions(1);
                    if(!IsTouchingWall) castSideCollisions(-1);
                } else {
                    castSideCollisions(_movingRight);
                }
            }
            castDownCollisions();

        }

        void releaseGrab() {

            float angle = (Vector2.SignedAngle(Vector2.down, (Velocity)) + 180) / (360 / 8);
            float des = 22.5f;
            if(LocalVelocity.x >= 0) {
                angle = Mathf.Floor(angle);
            } else {
                angle = Mathf.Ceil(angle);
                des *= -1;
            }
            //angle = Mathf.Round(angle);

            angle *= (360 / 8);

            Debug.DrawRay(_grabedAnchor.Anchor, Vector2.up.Rotate(angle + des), Color.red, 5);

            // _velocity = _velocity.normalized * _velocity.magnitude * 1.25f;

            Velocity = Vector2.up.Rotate(angle + des) * Velocity.magnitude * 1.5f;

        }

        void applyVelocity() {
            Position += Velocity * Time.deltaTime;
        }

        void applyDrag() {
            
            if(_inputForce == 0 && (IsGrounded || IsGrabbed)) {

                Vector2 dragForce = Velocity.normalized * _drag * Time.deltaTime;

                if(IsGrabbed) {
                    dragForce *= .1f;
                }

                if(dragForce.magnitude > Velocity.magnitude) {
                    dragForce = Velocity;
                }
                Velocity -= dragForce;
            }

            if(_hasNoVelocity) {

                Velocity = Vector2.zero;
            }

            _inputForce = 0;
        }

        void castSideCollisions(int dir) {

           /* if(_movingRight == 0) {
                _wallHit = Physics2D.Raycast(Position - BodyRight * _useWidth, BodyRight, _useWidth * 2 ).collider;
                Debug.Log("HIT R");
            } else {*/
            _wallHit = null;
            _sideRayBox.cast(Position, BodyRight * dir, UseWidth / 2 + Mathf.Abs(Velocity.x) * Time.deltaTime);
                
            foreach(var hit in _sideRayBox.Hits) {
                if(hit.collider != null) {
                    Position += BodyRight * dir * (hit.distance - (UseWidth / 2));

                    cancelVelocity(hit.normal);
                    //Velocity -= (Vector2)Vector3.Project(Velocity, hit.normal);

                    _wallHit = hit.collider;
                    break;
                }
            }

            //}

        }

        void castDownCollisions() {

            if(_movingUp < 0) {
                _hit = Physics2D.Raycast(Position, Vector3.down, UseHeight / 2f - Velocity.y * Time.deltaTime + .01f);
            } else {
                _hit = Physics2D.Raycast(NextFramePosition, Vector3.down, UseHeight / 2f);
            }

            if(_hit.collider != null) {
                if(Vector2.Angle(_hit.normal, _groundCollision) < 70) {

                    //Debug.Log("HIT DOWN");

                    Position -= Vector2.up * (_hit.distance - UseHeight / 2);
                    if(_movingUp < 0) {
                        Position -= Vector2.up * Velocity.y * Time.deltaTime;


                        //_velocity = Vector3.Project(_velocity, Vector3.Cross(_hit.normal, Vector3.forward));
                    }

                    if(!IsGrounded) IsLanding = true;

                    _groundCollision = _hit.normal;
                } else {
                    _groundCollision = Vector2.zero;
                }
            } else {
                _groundCollision = Vector2.zero;
            }

        }

        void limitGrabbedDirection() {

            Vector2 normal = (_grabedAnchor.Anchor - (Position)).normalized;

            float distance = Vector2.Distance(_grabedAnchor.Anchor, Position);

            if(Vector2.Distance(_grabedAnchor.Anchor, Position + (BodyUp * _usePointHeight)) > _grabDistance) {
                Position += normal * (distance - (_grabDistance + _usePointHeight));

                //if(Vector2.Angle(_velocity, -normal) < 90) {
                    Velocity = Vector3.Project(Velocity, Vector2.Perpendicular(-normal));
                //}

                _timeGrabed += Time.deltaTime;

                if(!_grabedAnchor.IsOnWall) {
                    transform.up = Vector2.Lerp(BodyUp, normal, Vector2.Distance(BodyUp, normal) * _timeGrabed * 50);
                }
                

            } else {
                _timeGrabed = 0;
            }



        }

        public static Vector2 RotatePointAroundPivot(Vector2 v, Vector2 pivot, float angle) {
            Vector2 dir = v - pivot; // get point direction relative to pivot
            //dir = Quaternion.Euler(0, 0, angle) * dir; // rotate it
            dir = dir.Rotate(angle);
            v = dir + pivot; // calculate rotated point
            return v; // return it
        }

        void checkGrabCorners() {
            
            if(GrabToEdges && !IsGrabbed) {
                _grabedAnchor = _cornerDetector.cast(NextFramePosition + BodyUp * _usePointHeight);

                if(_grabedAnchor.Anchor.y <= (NextFramePosition + BodyUp * _usePointHeight).y ||
                    Vector2.Angle(_grabedAnchor.GroundDirection, BodyRight * _lookDirection) > 90) {
                    _grabedAnchor = Corner.EMPTY;
                } else if(_grabedAnchor.IsValid) {
                    Velocity *= .75f;
                }

            } else if(!GrabToEdges && IsGrabbed) {
                clearGrabedAnchor();
            }
        }

        void clearGrabedAnchor() {

            _isReleasingGrab = true;
            _timeGrabed = 0;
            _grabedAnchor = Corner.EMPTY;
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(Position - Vector2.up * _height / 2, Vector2.up * _height);
            Gizmos.DrawRay(Position - Vector2.right * _width / 2, Vector2.right * _width);

            Gizmos.DrawWireCube(Position, new Vector2(_width, _height));

            Gizmos.color = Color.cyan;
            Gizmos.DrawWireCube(Position, new Vector2(_crouchWidth, _crouchHeight));

            Gizmos.color = Color.white;
            Gizmos.DrawWireCube(Position, new Vector2(UseWidth, UseHeight));

            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(Position + (BodyUp * _usePointHeight), _grabDistance);


            _grabedAnchor.drawGizmos();

            Gizmos.color = Color.black;
            Gizmos.DrawRay(Position, LocalVelocity);
            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(Position, Velocity);

            Gizmos.color = Color.red;
            Gizmos.DrawRay(Position, BodyRight * _lookDirection);
        }

    }


}
