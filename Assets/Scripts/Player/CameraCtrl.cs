﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastAscend.Player {
    public class CameraCtrl : MonoBehaviour {

        [SerializeField]
        Transform _target = null;

        private void LateUpdate() {
            transform.position = _target.position - Vector3.forward * 10;
        }
    }
}