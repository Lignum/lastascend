﻿using LastAscend.Entities;
using LastAscend.Physics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastAscend.Player {

    [RequireComponent(typeof(PhysicEntity))]
    public class PlayerCtrl : Object2D {

        PhysicEntity _entity;

        [Header("Movement")]
        [SerializeField]
        float _runSpeed = 2;
        [SerializeField]
        float _airMovementSpeed = .5f;
        [SerializeField]
        float _crouchSpeed = 3;

        [Header("Walls")]
        [SerializeField]
        float _slideTime = 1;
        float _slidingTime = 0;
        [SerializeField]
        float _slideSpeedMult = .5f;

        [Header("Jumps")]
        [SerializeField]
        float _jumpForce = 2;
        [SerializeField]
        float _wallJumpVerticalForce = 3;
        [SerializeField]
        float _wallJumpSideForce = 3;

        [SerializeField]
        float _wallDistanceToWallJump = .1f;
        Vector2 _closestWallNormal = Vector2.zero;
        float _wallDistance;

        
        [Header("Grabs")]
        [SerializeField]
        float _grabTurnForce = 1.5f;
        [SerializeField]
        float _climbSpeed = 2;

        CollisionRayBox _sideRayBox;
        bool _canVerticalWallJump = false;

        private void Start() {
            _entity = GetComponent<PhysicEntity>();

            _sideRayBox = new CollisionRayBox(1, 20);
        }


        private void Update() {

            _entity.GrabToEdges = Input.GetKey(KeyCode.K);

            if(Input.GetKey(KeyCode.L)) {
                _entity.addClimb(_climbSpeed);
            } else if(Input.GetKeyUp(KeyCode.L)) {
                _entity.stopClimb();
            }

            //Debug.Log(_entity.IsTouchingWall);
            if(_entity.IsClimbing) {
                return;
            }
            
            if(Input.GetAxisRaw("Vertical") < 0) {
                _entity.crouch(_crouchSpeed);
            } else {
                _entity.crouch(-_crouchSpeed);
            }

            if(_entity.TimeSinceGrounded < .05f) {
                _canVerticalWallJump = true;
                _entity.run(Input.GetAxisRaw("Horizontal") * _runSpeed);

                if(Input.GetKeyDown(KeyCode.Space)) {
                    _entity.addForce(Vector2.up * _jumpForce);
                }

            }else if(_entity.IsGrabbed) {
                _entity.run(Input.GetAxisRaw("Horizontal") * _grabTurnForce);
            }else if(_closestWallNormal != Vector2.zero) {

                slideOnWall();

            } else {//Is in the air
                _entity.run(Input.GetAxisRaw("Horizontal") * _airMovementSpeed);
            }

            if(_closestWallNormal == Vector2.zero) {
                _slidingTime = 0;
            }
            

            checkDistanceToWall();
        }

        void checkDistanceToWall() {

            _wallDistance = _wallDistanceToWallJump;
            _closestWallNormal = Vector2.zero;

            _sideRayBox.setSize(_entity.UseHeight / 1.5f);
            HashSet<RaycastHit2D> hits = _sideRayBox.cast(Position, _wallDistanceToWallJump + Mathf.Abs(_entity.LocalVelocity.x * Time.deltaTime), Vector2.right, Vector2.left);

            int hitsNum = 0;

            foreach(var item in hits) {

                if(item.collider != null && item.distance <= _wallDistance) {
                    _closestWallNormal = item.normal;
                    _wallDistance = item.distance;

                    hitsNum++;
                }
            }
            if(hitsNum < _sideRayBox.NumOfRays / 2) {
                _closestWallNormal = Vector2.zero;
            }

        }

        void slideOnWall() {

            if(Input.GetKeyDown(KeyCode.Space) && _slidingTime <= _slideTime) {

                if(Input.GetKey(KeyCode.S)) {
                    _slidingTime = _slideTime + 1;

                }else if(Input.GetKey(KeyCode.W) && _canVerticalWallJump) {
                    _entity.cancelVelocity(Vector2.up);
                    _entity.addForce(Vector2.up * _wallJumpVerticalForce);
                    _canVerticalWallJump = false;

                } else if(!Input.GetKey(KeyCode.W)) {
                    _entity.cancelVelocity(Vector2.up);
                    _entity.addForce(Vector2.up * _wallJumpVerticalForce);
                    _entity.addForce(_closestWallNormal * _wallJumpSideForce);
                    _canVerticalWallJump = true;
                }
            } else {
                if(_slidingTime <= _slideTime && (_entity.Velocity.x * _closestWallNormal.x < 0 || _entity.Velocity.y < 0)) {
                    _entity.cancelVelocity(Vector2.up, _slideSpeedMult);
                    _slidingTime += Time.deltaTime;
                }else if(_slidingTime <= _slideTime) {
                    _slidingTime = 0;
                }
            }
        }



        //DEBUG
        Vector2 _lastPosition;
        private void OnDrawGizmos() {

            if(_entity != null) {
                if(Input.GetKeyDown(KeyCode.Space)) {
                    Gizmos.color = Color.red;
                    Gizmos.DrawRay(Position, Vector2.up * .1f);
                }

                if(_entity.IsGrounded) {
                    Gizmos.color = Color.green;
                } else if(_entity.TimeSinceGrounded < .05f) {
                    Gizmos.color = Color.yellow;
                } else {
                    Gizmos.color = Color.red;
                }
                //Gizmos.DrawLine(Position, _entity.NextFramePosition);
                Gizmos.DrawLine(_lastPosition, Position);
                _lastPosition = Position;
            }
        }
    }
}