﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct TransformPose {

    public string boneName { get; private set; }

    Vector3 position;
    Vector3 scale;
    Quaternion rotation;

    public TransformPose(Transform baseT, string boneName) {
        this.boneName = boneName;

        position = baseT.localPosition;
        scale = baseT.localScale;
        rotation = baseT.localRotation;
    }

    public void applyToTransform(Transform target) {
        target.localPosition = position;
        target.localRotation = rotation;
        target.localScale = scale;
    }

    public static TransformPose lerp(TransformPose t1, TransformPose t2, float e) {
        TransformPose res = new TransformPose();
        res.position = Vector3.LerpUnclamped(t1.position, t2.position, e);
        res.scale = Vector3.LerpUnclamped(t1.scale, t2.scale, e);
        res.rotation = Quaternion.LerpUnclamped(t1.rotation, t2.rotation, e);

        return res;
    }
}