﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SkeletonPose  {

    [SerializeField]
    float priority;

    public Dictionary<string, TransformPose> Bones { get; private set; }
    
    public SkeletonPose(float priority, Dictionary<string, TransformPose> bones) {
        this.priority = Mathf.Clamp(priority, .001f, 1);
        this.Bones = bones;
    }

    public static SkeletonPose lerp(SkeletonPose t1, SkeletonPose t2, float e) {
        SkeletonPose res = new SkeletonPose();
        res.Bones = new Dictionary<string, TransformPose>();

        foreach(var item in t1.Bones) {
            res.Bones.Add(item.Key, TransformPose.lerp(item.Value, t2.Bones[item.Key], e * (t2.priority / t1.priority)));
        }

        Debug.Log($"{e} -> {e * (t2.priority / t1.priority)}");
        res.priority = Mathf.Lerp(t1.priority, t2.priority, e );

        return res;
    }

    public static SkeletonPose lerpArray(float e, params SkeletonPose[] skeletons) {
        SkeletonPose res = new SkeletonPose();
        res.Bones = new Dictionary<string, TransformPose>();

        if(e <= 0) {
            return skeletons[0];
        } else if(e < skeletons.Length - 1) {
           return lerp(skeletons[(int)e], skeletons[(int)e + 1], e - (int)e);
        } else {
            return skeletons[skeletons.Length - 1];
        }
        
    }
}
