﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPose : MonoBehaviour{

    [SerializeField]
    Transform boneBase;

    [SerializeField, Range(.001f, 1)]
    float priority = 1;

    public SkeletonPose skeleton { get; private set; }

    // Start is called before the first frame update
    public SkeletonPose getSkeleton(){
        Transform[] boneTrans = boneBase.GetComponentsInChildren<Transform>();
        Dictionary<string, TransformPose> bones = new Dictionary<string, TransformPose>(boneTrans.Length);

        foreach(var item in boneTrans) {
            bones.Add(item.name, new TransformPose(item, item.name));
        }

        skeleton = new SkeletonPose(priority, bones);

        return skeleton;
    }



}

