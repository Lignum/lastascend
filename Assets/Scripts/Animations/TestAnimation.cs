﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAnimation : MonoBehaviour{

    public AnimationPose cli1, cli2, cli3, cli4, cli5;
    public AnimationPose crouch;

    SkeletonPose Scli1, Scli2, Scli3, Scli4, Scli5, Scrouch;

    [SerializeField]
    Transform boneBase;

    [SerializeField]
    float lerp;

    [SerializeField]
    float crouchLerp;

    public Dictionary<string, Transform> bones;

    // Start is called before the first frame update
    void Start() {
        Transform[] boneTrans = boneBase.GetComponentsInChildren<Transform>();
        bones = new Dictionary<string, Transform>(boneTrans.Length);

        foreach(var item in boneTrans) {
            bones.Add(item.name, item);
        }

        Scli1 = cli1.getSkeleton();
        Scli2 = cli2.getSkeleton();
        Scli3 = cli3.getSkeleton();
        Scli4 = cli4.getSkeleton();
        Scli5 = cli5.getSkeleton();

        Scrouch = crouch.getSkeleton();
    }

    // Update is called once per frame
    void Update(){
        /*if(Input.GetKey(KeyCode.A)) {
            setPose(cli1);
        }
        if(Input.GetKey(KeyCode.S)) {
            setPose(cli2);
        }
        if(Input.GetKey(KeyCode.D)) {
            setPose(cli3);
        }
        if(Input.GetKey(KeyCode.F)) {
            setPose(cli4);
        }*/

        lerp += Time.deltaTime;
        lerp %= 4;

        SkeletonPose walkAni = SkeletonPose.lerpArray(lerp, Scli1, Scli2, Scli3, Scli4, Scli5);

        setPose(SkeletonPose.lerp(walkAni, Scrouch, crouchLerp));
        //setPose(crouch.skeleton);
    }

    void setPose(SkeletonPose pose) {

        foreach(var item in pose.Bones) {
            item.Value.applyToTransform(bones[item.Key]);
        }

    }
}
